#include <iostream>
using namespace std;

class Student
{
public:
    string m_jmeno = "";
    int m_vek = 0;
    float m_prumer = 0.0f;
    bool m_stipko = false;

    void SetJmeno(string noveJmeno)
    {
        m_jmeno = noveJmeno;
    }

    void SetVek(int vek)
    {
        if (vek<0)
        {
            cout << "Pozadovna hodnota neni vek" << endl;
        }
        else
        {
            m_vek = vek;
        }
    }

    int GetVek()
    {
        return m_vek;
    }

    bool GetStipko()
    {
        return m_stipko;
    }

    void PrintInfo()
    {
        cout << "--STUDENT--" << endl;
        cout << "jmeno: " << m_jmeno << endl;
        cout << "prumer: " << m_prumer << endl;
        cout << "stipko: " << m_stipko << endl;
        cout << "vek: " << m_vek << endl;
        cout << "-----------" << endl;
    }
};

int main() {
    Student* s1 = new Student();
    s1->m_jmeno = "Adam";
    s1->m_prumer = 1.8f;
    s1->SetVek(-20);
    int pomVek =  s1->GetVek();
    s1->PrintInfo();
    delete s1;
    return 0;
}