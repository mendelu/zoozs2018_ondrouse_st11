#ifndef Helma_H
#define Helma_H

#include <iostream>

using namespace std;


	class Helma {

	protected:
		int m_velikost;

	public:
		Helma(int velikost);

		virtual int getBonusObrany() = 0;

		void printInfo();
	};

#endif // Helma_H
