#include "KrouzkoveBrneni.h"

KrouzkoveBrneni::KrouzkoveBrneni(int vaha, int odolnost, int ohebnost):Brneni(vaha, odolnost) {
	m_ohebnost = ohebnost;
}

int KrouzkoveBrneni::getBonusObrany() {
	return m_odolnost;
}

int KrouzkoveBrneni::getBonusUtoku() {
	return m_odolnost/2 + m_ohebnost;
}

void KrouzkoveBrneni::printInfo() {
	Brneni::printInfo();
    cout << "-typ: krouzkove brneni" << endl;
    cout << "-odolnost: " << m_odolnost << endl;
    cout << "-ohebnost: " << m_ohebnost << endl;
}
