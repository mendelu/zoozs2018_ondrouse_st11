//
// Created by ondrouse on 05.12.2018.
//

#include "LehkoodenecBuilder.h"



void LehkoodenecBuilder::BuildHelma(int velikostHelmy)
{
    LehkaUtocnaHelma* lh = new LehkaUtocnaHelma(velikostHelmy);
    m_rytir->setHelma(lh);
}

void LehkoodenecBuilder::BuildZbroj(int vaha, int odolnost, int ohebnost, int velikostPlatu)
{
    KrouzkoveBrneni* kb = new KrouzkoveBrneni(vaha, odolnost, ohebnost);
    m_rytir->setZbroj(kb);
}