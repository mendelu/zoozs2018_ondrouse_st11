//
// Created by ondrouse on 05.12.2018.
//

#ifndef PROJEKTZACATEK_RYTIRBUILDER_H
#define PROJEKTZACATEK_RYTIRBUILDER_H


#include "Rytir.h"

class RytirBuilder {
protected:
    Rytir* m_rytir;
public:
    void CreateRytir(string jmeno, int sila);
    virtual void BuildZbroj(int vaha, int odolnost, int ohebnost, int velikostPlatu)=0;
    virtual void BuildHelma(int velikostHelmy)=0;
    Rytir* GetRytir();
};


#endif //PROJEKTZACATEK_RYTIRBUILDER_H
