//
// Created by ondrouse on 05.12.2018.
//

#include "RytirDirector.h"

RytirDirector::RytirDirector(RytirBuilder* builder)
{
    m_builder = builder;
}

void RytirDirector::SetBuilder(RytirBuilder* builder)
{
    m_builder = builder;
}

Rytir* RytirDirector::GetRytir(string jmeno, int sila, int vaha, int odolnost,
        int ohebnost, int velikostPlatu, int velikostHelmy)
{
    m_builder->CreateRytir(jmeno, sila);
    m_builder->BuildZbroj(vaha, odolnost, ohebnost, velikostPlatu);
    m_builder->BuildHelma(velikostHelmy);
    return m_builder->GetRytir();
}