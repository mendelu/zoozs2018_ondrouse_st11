#include <iostream>

#include "Rytir.h"
#include "LehkaUtocnaHelma.h"
#include "KrouzkoveBrneni.h"
#include "RytirDirector.h"
#include "LehkoodenecBuilder.h"


using namespace std;

int main()
{
    Rytir* r1 = new Rytir("Artus",25);
    LehkaUtocnaHelma* lum = new LehkaUtocnaHelma(3);
    r1->setHelma( lum );
    KrouzkoveBrneni* kb = new KrouzkoveBrneni(10,5,1);
    r1->setZbroj( kb );
    r1->print();

    delete r1;
    delete lum;
    delete kb;

    LehkoodenecBuilder* lb = new LehkoodenecBuilder();
    RytirDirector* reditel = new RytirDirector(lb);
    Rytir* r0 = reditel->GetRytir("artus", 100, 85, 10, 10, 20, 3);
    Rytir* r2 = reditel->GetRytir("shrek", 100, 85, 10, 10, 30, 3);
    Rytir* r3 = reditel->GetRytir("cesar", 100, 85, 10, 10, 40, 3);
    Rytir* r4 = reditel->GetRytir("napoleon", 100, 85, 10, 50, 5, 3);
    Rytir* r5 = reditel->GetRytir("artus", 100, 85, 10, 10, 60, 3);

    r5->print();
    return 0;
}
