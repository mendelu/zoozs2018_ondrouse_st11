#ifndef LehkaUtocnaHelma_H
#define LehkaUtocnaHelma_H

#include "Helma.h"


	class LehkaUtocnaHelma : public Helma {


	public:
		LehkaUtocnaHelma(int velikost);

		int getBonusObrany();

		void printInfo();
	};

#endif // LehkaUtocnaHelma_H
