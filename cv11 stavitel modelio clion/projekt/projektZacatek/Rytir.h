#ifndef Rytir_H
#define Rytir_H

#include <iostream>
#include "Brneni.h"
#include "Helma.h"
using namespace std;


	class Rytir {

	private:
		string m_jmeno;
		int m_sila;
		Brneni* m_zbroj;
		Helma* m_helma;

	public:
		Rytir(string jmeno, int sila);

		int getUtok();

		int getObrana();

		void setZbroj(Brneni* brneni);

		void setHelma(Helma* helma);

		void print();
	};

#endif // Rytir_H
