cmake_minimum_required(VERSION 3.12)
project(cv12Command)

set(CMAKE_CXX_STANDARD 14)

add_executable(cv12Command main.cpp Lektvar.cpp Lektvar.h Prikaz.h hRDINA.cpp hRDINA.h PrikazProhledni.cpp PrikazProhledni.h PrikazUpij.cpp PrikazUpij.h PrikazVypijVse.cpp PrikazVypijVse.h)