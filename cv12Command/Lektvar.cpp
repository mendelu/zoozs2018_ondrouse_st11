//
// Created by ondrouse on 12.12.2018.
//

#include "Lektvar.h"

Lektvar::Lektvar(int bonus, string popis)
{
    m_bonusZivota = bonus;
    m_popis = popis;
}

int Lektvar::GetBonusZivota()
{
    return m_bonusZivota;
}

string Lektvar::Getpopis()
{
    return m_popis;
}

void Lektvar::VyprazdniLektvar()
{
    m_bonusZivota =0;
}
