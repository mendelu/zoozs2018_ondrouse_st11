//
// Created by ondrouse on 12.12.2018.
//

#ifndef CV12COMMAND_LEKTVAR_H
#define CV12COMMAND_LEKTVAR_H

#include <iostream>
using namespace std;

class Lektvar
{
private:
    int m_bonusZivota;
    string m_popis;
public:
    Lektvar(int bonus, string popis);
    void VyprazdniLektvar();
    int GetBonusZivota();
    string Getpopis();
};


#endif //CV12COMMAND_LEKTVAR_H
