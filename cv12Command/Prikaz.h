//
// Created by ondrouse on 12.12.2018.
//

#ifndef CV12COMMAND_PRIKAZ_H
#define CV12COMMAND_PRIKAZ_H

#include "hRDINA.h"
#include "Lektvar.h"

class Prikaz
{
public:
    virtual void PouzijLektvar(hRDINA* hrdina, Lektvar* lektvarek)=0;
    virtual string GetPopis()=0;
};


#endif //CV12COMMAND_PRIKAZ_H
