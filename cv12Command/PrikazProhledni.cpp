//
// Created by ondrouse on 12.12.2018.
//

#include "PrikazProhledni.h"

void PrikazProhledni::PouzijLektvar(hRDINA *hrdina, Lektvar *lektvarek)
{
    cout << "Vytahl jsi z batohu tento lektvar: " << endl;
    lektvarek->Getpopis();
    cout << "Koukas koukas, nevykoukas." << endl;
}

string PrikazProhledni::GetPopis()
{
    return "Pouze prohlednout.";
}
