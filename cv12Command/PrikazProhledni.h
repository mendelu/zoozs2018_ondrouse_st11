//
// Created by ondrouse on 12.12.2018.
//

#ifndef CV12COMMAND_PRIKAZPROHLEDNI_H
#define CV12COMMAND_PRIKAZPROHLEDNI_H

#include "Prikaz.h"

class PrikazProhledni: public Prikaz
{
    void PouzijLektvar(hRDINA* hrdina, Lektvar* lektvarek);
    string GetPopis();
};


#endif //CV12COMMAND_PRIKAZPROHLEDNI_H
