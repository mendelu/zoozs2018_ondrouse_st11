//
// Created by ondrouse on 19.12.2018.
//

#ifndef CV12COMMAND_PRIKAZUPIJ_H
#define CV12COMMAND_PRIKAZUPIJ_H


#include "Prikaz.h"

class PrikazUpij : public Prikaz
{
    void PouzijLektvar(hRDINA* hrdina, Lektvar* lektvarek);
    string GetPopis();
};


#endif //CV12COMMAND_PRIKAZUPIJ_H
