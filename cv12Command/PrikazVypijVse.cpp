//
// Created by ondrouse on 19.12.2018.
//

#include "PrikazVypijVse.h"

void PrikazVypijVse::PouzijLektvar(hRDINA *hrdina, Lektvar *lektvarek)
{
    int pom = lektvarek->GetBonusZivota();
    hrdina->ZmenZivotokolik( pom );
    lektvarek ->VyprazdniLektvar();
    cout << "Pekne jsi to vyzunkl." << endl;
}

string PrikazVypijVse::GetPopis()
{
    return "Vypij cely lektvar a zahod ho.";
}