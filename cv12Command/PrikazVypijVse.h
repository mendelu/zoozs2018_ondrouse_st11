//
// Created by ondrouse on 19.12.2018.
//

#ifndef CV12COMMAND_PRIKAZVYPIJVSE_H
#define CV12COMMAND_PRIKAZVYPIJVSE_H


#include "Prikaz.h"

class PrikazVypijVse : public Prikaz
{
        void PouzijLektvar(hRDINA* hrdina, Lektvar* lektvarek);
        string GetPopis();
};


#endif //CV12COMMAND_PRIKAZVYPIJVSE_H
