//
// Created by ondrouse on 12.12.2018.
//

#include "hRDINA.h"
#include "Prikaz.h"

void hRDINA::PrintInfo()
{}

hRDINA::hRDINA(int zivot)
{
    m_zivot = zivot;
}

void hRDINA::NaucNovyPrikaz(Prikaz* prikaz)
{
    m_prikazy.push_back(prikaz);
}

void hRDINA::Seberlektvar(Lektvar* lektvarek)
{
    m_lektvary.push_back(lektvarek);
}

void hRDINA::VypisLektvary()
{
    cout << "Lektvary:" << endl;
    for (int i=0; i<m_lektvary.size(); i++)
    {
        cout << i << ". " << m_lektvary.at(i)->Getpopis() << endl;
    }
}

void hRDINA::VypisPrikazy()
{
    cout << "Prikazy:" << endl;
    for (int i=0; i<m_prikazy.size(); i++)
    {
        cout << i << ". " << m_prikazy.at(i)->GetPopis() << endl;
    }
}

void hRDINA::ZmenZivotokolik(int okolik)
{
    m_zivot += okolik;
}

void hRDINA::PouzijLektvar()
{
    int kteryLektvar = 0;
    VypisLektvary();
    cout << "Ktery lektvar chces pouzit: ";
    cin >> kteryLektvar;

    int kteryPrikaz = 0;
    VypisPrikazy();
    cout << "Ktery prikaz chces provest: ";
    cin >> kteryPrikaz;

    m_prikazy.at(kteryPrikaz)->PouzijLektvar(this,m_lektvary.at(kteryLektvar));

    if (m_lektvary.at(kteryLektvar)->GetBonusZivota() == 0)
    {
        m_lektvary.erase(m_lektvary.begin()+kteryLektvar);
    }
}
