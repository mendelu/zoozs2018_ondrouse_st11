//
// Created by ondrouse on 12.12.2018.
//

#ifndef CV12COMMAND_HRDINA_H
#define CV12COMMAND_HRDINA_H


#include <c++/vector>
#include "Lektvar.h"

class Prikaz;

class hRDINA
{
private:
    int m_zivot;
    vector<Lektvar*> m_lektvary;
    vector<Prikaz*> m_prikazy;
    void VypisLektvary();
    void VypisPrikazy();
public:
    hRDINA(int zivot);
    void Seberlektvar(Lektvar* lektvarek);
    void ZmenZivotokolik(int okolik);
    void NaucNovyPrikaz(Prikaz* prikaz);
    void PouzijLektvar();
    void PrintInfo();
};


#endif //CV12COMMAND_HRDINA_H
