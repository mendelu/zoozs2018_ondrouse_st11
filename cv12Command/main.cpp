#include <iostream>
#include "hRDINA.h"
#include "PrikazProhledni.h"
#include "PrikazUpij.h"
#include "PrikazVypijVse.h"

int main() {

    hRDINA* artus = new hRDINA(100);

    Lektvar* l1 = new Lektvar(20,"zelena brecka");
    Lektvar* l2 = new Lektvar(40,"cerna bublajici stavicka");
    artus->Seberlektvar(l1);
    artus->Seberlektvar(l2);

    Prikaz* p1 = new PrikazProhledni();
    artus->NaucNovyPrikaz(p1);

    Prikaz* p2 = new PrikazUpij();
    artus->NaucNovyPrikaz(p2);

    Prikaz* p3 = new PrikazVypijVse();
    artus->NaucNovyPrikaz(p3);

    artus->PouzijLektvar();
    artus->PrintInfo();

    delete artus;
    return 0;
}