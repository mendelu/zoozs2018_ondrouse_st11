#include <iostream>
using namespace std;

class Balik
{
public:
    string m_prijemce = "";
    string m_odesilatel = "";
    float m_vaha = 0.0f;
    int m_dobirka = 0;

    Balik(string prijemce, string odesilatel, float vaha, int dobirka)
    {
        m_prijemce = prijemce;
        m_odesilatel = odesilatel;
        m_dobirka = dobirka;
        m_vaha = vaha;
    }

    Balik(string prijemce, string odesilatel)
    {
        m_prijemce = prijemce;
        m_odesilatel = odesilatel;
        m_dobirka = 0;
        m_vaha = 0.0f;
    }

    Balik(string prijemce, string odesilatel, float vaha)
    {
        m_prijemce = prijemce;
        m_odesilatel = odesilatel;
        m_dobirka = 0;
        m_vaha = vaha;
    }

    void SetVaha(float vaha)
    {
        m_vaha = vaha;
    }

    void PrintInfo()
    {
        cout << "--BALIK--" << endl;
        cout << "prijemce: " << m_prijemce << endl;
        cout << "odesilatel: " << m_odesilatel << endl;
        cout << "vaha: " << m_vaha << endl;
        cout << "dobirka: " << m_dobirka << endl;
        cout << "---------" << endl;

    }
};

int main() {
    Balik* b1 = new Balik("Adam","Eva",3.2,150);
    b1->SetVaha(3.5);
    b1->PrintInfo();
    delete b1;
    return 0;
}