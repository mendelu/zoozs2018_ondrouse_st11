#include <iostream>
using namespace std;

class Auto
{
private:
    string m_posledniRidic = "";
    int m_zbytkovaHodnota = 0;
    int m_ujetoKm = 0;
    int m_amortizaceNaKm = 20;

    void vypocitejZbytkovouHodnotu(int posledniJizdaKm)
    {
        float koef = (float)m_ujetoKm / 100000;
        m_zbytkovaHodnota = m_zbytkovaHodnota - (posledniJizdaKm * m_amortizaceNaKm * koef);
    }

public:
    Auto(int porizovaciCena)
    {
        m_posledniRidic = "";
        m_amortizaceNaKm = 20;
        m_ujetoKm = 0;
        m_zbytkovaHodnota = porizovaciCena;
    }

    Auto(int porizovaciCena, int amortizaceNaKm)
    {
        m_posledniRidic = "";
        m_amortizaceNaKm = amortizaceNaKm;
        m_ujetoKm = 0;
        m_zbytkovaHodnota = porizovaciCena;
    }

    void PridejJizdu(string jmeno, int kolikUjel)
    {
        m_posledniRidic = jmeno;
        m_ujetoKm = m_ujetoKm + kolikUjel; // m_ujetoKm += kolikUjel;
        vypocitejZbytkovouHodnotu(kolikUjel);
    }

    string GetPosledniRidic()
    {
        return m_posledniRidic;
    }

    bool JekOdpisu()
    {
        if (m_zbytkovaHodnota<0)
        {
            return true;
        }
        else
        {
            return false;
        }
        //return (m_zbytkovaHodnota<0);
    }

    void PrintInfo()
    {
        cout << "--Auto--" << endl;
        cout << "posledni ridic: " << m_posledniRidic << endl;
        cout << "zbytkova hodnota: " << m_zbytkovaHodnota << endl;
        cout << "ujeto km: " << m_ujetoKm << endl;
        if (JekOdpisu())
        {
            cout << "je k odpisu " << endl;
        }
        else
        {
            cout << "neni k odpisu " << endl;
        }
        cout << "--------" << endl;
    }

};

int main() {

    Auto* a = new Auto(500000,35000);
    a->PridejJizdu("Pepa",150);
    a->PridejJizdu("Alenka",350);
    a->PrintInfo();
    cout << a->JekOdpisu() << endl;
    delete a;
    return 0;
}