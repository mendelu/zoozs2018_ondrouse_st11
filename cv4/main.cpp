#include <iostream>
using namespace std;

class ErrorLog
{
private:
    static ErrorLog* s_errorLog;
    string m_errors = "";
    ErrorLog()
    {
        m_errors = "logy za behu programu: \n";
    }
public:
    void LogIt(string odkud, string co)
    {
        m_errors += "- " + odkud + "  "+ co + "\n";
    }

    string GetErrors()
    {
        return m_errors;
    }

    static ErrorLog* GetErrorLogObject()
    {
        if (s_errorLog == nullptr)
        {
            s_errorLog = new ErrorLog();
        }
        return s_errorLog;
    }
};
ErrorLog* ErrorLog::s_errorLog = nullptr;

class Zviratko
{
private:
    int m_vaha = 0;
    int m_kalorie = 0;
public:
    Zviratko(int kalorie, int vaha)
    {
        m_kalorie = kalorie;
        m_vaha = vaha;
        ErrorLog* pom = ErrorLog::GetErrorLogObject();
        pom->LogIt("Zviratko konstruktor","dalsi zvire na svete");
    }

    int GetVaha()
    {
        int pom = m_vaha;
        m_vaha = 0;
        return pom;
    }

    int GetKalorie()
    {
        int pom = m_kalorie;
        m_kalorie =0;
        return pom;
    }

    void PrintInfo()
    {
        cout << "Zviratko" << endl;
        cout << "vaha: " << m_vaha << endl;
        cout << "kalorie: " << m_kalorie << endl;
        cout << "--------" << endl;
    }
};

class Drak
{
private:
    int m_sezranoVahy =0;
    int m_sezranoKalorii = 0;
public:
    void Sezer(Zviratko* obed)
    {
        m_sezranoKalorii += obed->GetKalorie();
        m_sezranoVahy += obed->GetVaha();

        ErrorLog* pom = ErrorLog::GetErrorLogObject();
        pom->LogIt("Drak metoda Sezer","sezral zvire");
    }

    void PrintInfo()
    {
        cout << "Drak- sezrano " << m_sezranoVahy << "g, energie: " << m_sezranoKalorii << "kJ. " << endl;
    }
};

int main() {
    Zviratko* tweety = new Zviratko(25, 150);
    Zviratko* kacerDonald = new Zviratko(30, 180);
    Drak* smak = new Drak();

    smak->Sezer(tweety);
    smak->Sezer(kacerDonald);
    smak->PrintInfo();
    tweety->PrintInfo();
    kacerDonald->PrintInfo();

    delete tweety;
    delete kacerDonald;
    delete smak;

    ErrorLog* pom = ErrorLog::GetErrorLogObject();
    cout << pom->GetErrors() << endl;
    return 0;
}