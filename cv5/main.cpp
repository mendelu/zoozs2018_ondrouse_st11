#include <iostream>
#include <array>

using namespace std;

class Kontejner
{
private:
    string m_majitel;
    int m_vaha;
    string m_obsah;
public:
    Kontejner(string majitel, int vaha, string obsah)
    {
        m_majitel = majitel;
        m_obsah = obsah;
        m_vaha = vaha;
    }

    void PrintInfo()
    {
        cout << "Kontejner: " << m_majitel << " " << m_obsah << " " << m_vaha << endl;
    }

    int GetVaha()
    {
        return m_vaha;
    }

    string GetMajitel()
    {
        return m_majitel;
    }

    string GetObsah()
    {
        return m_obsah;
    }
};

class Patro
{
private:
    string m_oznaceni;
    array<Kontejner*,10> m_pozice;

public:
    Patro(string oznaceni)
    {
        m_oznaceni = oznaceni;
        for(Kontejner* &k:m_pozice)
        {
            k = nullptr;
        }
    }

    void VypisObsah()
    {
        for (Kontejner* k:m_pozice)
        {
            if (k!= nullptr)
            {
                k->PrintInfo();
            }
        }
    }

    void PridejDoSkladu(Kontejner* co, int kam)
    {
        if (m_pozice.at(kam) == nullptr)
        {
            m_pozice.at(kam) = co;
        }
        else
        {
            cout << "Na danou pozici nelze pridat, uz tam je kontejner." << endl;
        }
    }
};

class Sklad
{
private:
    array<Patro*,5> m_patra;
public:
    Sklad()
    {
        int poradoveCislo = 0;
        for(Patro* &p:m_patra)
        {
            string oznaceni = "Patro" + to_string(poradoveCislo);
            poradoveCislo += 1;
            p = new Patro(oznaceni);
        }
    }

    ~Sklad()
    {
        for (Patro *&p:m_patra)
        {
            delete p;
        }
    }

    void VypisObsah()
    {
        cout << "\nObsah skladu: " << endl;

        for(Patro* p:m_patra){
            if (p != nullptr){
                p->VypisObsah();
            }
        }
    }
};

int main() {
    Kontejner* k1 = new Kontejner("Adam",500,"kola");
    Kontejner* k2 = new Kontejner("Bara",250,"macBook");
    Kontejner* k3 = new Kontejner("Cyril",100,"banany");

    Patro* p = new Patro("1.patro ");
    p->PridejDoSkladu(k1,0);
    p->PridejDoSkladu(k2,1);
    p->PridejDoSkladu(k3,5);
    p->VypisObsah();

    delete p;
    delete k1;
    delete k2;
    delete k3;
    return 0;
}