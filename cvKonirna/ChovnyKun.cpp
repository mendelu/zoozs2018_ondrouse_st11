#include "ChovnyKun.h"

ChovnyKun::ChovnyKun(string jmeno, int cena, int vaha, int stari)
    :Kun(jmeno, cena, vaha)
{
    m_stari = stari;
}

void ChovnyKun::SetStari(int stari)
{
    m_stari = stari;
}

void ChovnyKun::PrintInfo()
{
    cout << "--CHOVNY--" << endl;
    Kun::PrintInfo();
    cout << "stari: " << m_stari << endl;
    cout << "prodejniCena: " << GetProdejniCena() << endl;
    cout << "-----------" << endl;
}

int ChovnyKun::GetProdejniCena()
{
    int pom = m_zakladniCena;

    if (m_stari>=5)
    {
        pom = pom - 10000 * m_stari;
    }

    if (pom<50000)
    {
        pom = 50000;
    }
    return pom;
}