#ifndef CVKONIRNA_KONIRNA_H
#define CVKONIRNA_KONIRNA_H


#include <vector>
#include "Kun.h"

class Konirna {
private:
    vector<Kun*> m_stado;
    int m_stavPokladny;
public:
    Konirna(int penize);
    bool Nakup(Kun* koho);
    Kun* Prodej(string jmeno);
    void PrintInfo();
};


#endif //CVKONIRNA_KONIRNA_H
