#include "Kun.h"

void Kun::SetVaha(int vaha)
{
    m_vaha = vaha;
}

Kun::Kun(string jmeno, int cena, int vaha)
{
    m_jmeno = jmeno;
    m_zakladniCena = cena;
    m_vaha = vaha;
}

void Kun::PrintInfo()
{
    cout << "jmeno: " << m_jmeno << endl;
    cout << "vaha: " << m_vaha << endl;
    cout << "zakladni cena: " << m_zakladniCena << endl;
}

string Kun::GetJmeno()
{
    return  m_jmeno;
}