//
// Created by ondrouse on 21.11.2018.
//

#include "Trziste.h"

bool Trziste::ZaridObchod(Konirna *prodejce, Konirna *nakupujici, string jmeno)
{
    Kun* koho = prodejce->Prodej(jmeno);
    if (koho== nullptr)
    {
        cout << "prodejce nema kone daneho jmena" << endl;
        return false;
    }
    if (!nakupujici->Nakup(koho))
    {
        cout << "nakupujici na to nema" << endl;
        return false;
    }
    cout << "obchod uspesne dokoncen" << endl;
    return true;
}