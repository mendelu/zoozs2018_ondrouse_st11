#include "ZavodniKun.h"

ZavodniKun::ZavodniKun(string jmeno, int cena, int vaha, int pocetVyher)
    :Kun(jmeno, 500000, vaha)
{
    m_pocetVyhranych = pocetVyher;
}

void ZavodniKun::SetPocetVyher(int pocet)
{
    m_pocetVyhranych = pocet;
}

int ZavodniKun::GetProdejniCena()
{
    return m_zakladniCena + 10000*m_pocetVyhranych;
}

void ZavodniKun::PrintInfo()
{
    cout << "--ZAVODNI--" << endl;
    Kun::PrintInfo();
    cout << "pocetVyher: " << m_pocetVyhranych << endl;
    cout << "prodejniCena: " << GetProdejniCena() << endl;
    cout << "-----------" << endl;
}