#include <iostream>
#include "Kun.h"
#include "ZavodniKun.h"
#include "ChovnyKun.h"
#include "Konirna.h"
#include "Trziste.h"

int main() {
    //Kun* k = new Kun("Elias",255000,500);
    //k->PrintInfo();
    //delete k;

    ZavodniKun* z = new ZavodniKun("Pamela",300000,550,42);
    //z->PrintInfo();

    ChovnyKun* chk = new ChovnyKun("Zakara",650000,350,1);
    //chk->PrintInfo();

    Kun* kun1 = new ZavodniKun("Dharma",300000,550,42);
    //kun1->PrintInfo();
    //kun1->GetProdejniCena();

    Konirna* rancUPodkovy = new Konirna(2000111);
    rancUPodkovy->Nakup(z);
    rancUPodkovy->Nakup(chk);
    rancUPodkovy->Nakup(kun1);
    rancUPodkovy->Prodej("Dharma");
    rancUPodkovy->PrintInfo();

    Konirna* rancNaLouce = new Konirna(5000111);

    Trziste* t = new Trziste();
    t->ZaridObchod(rancUPodkovy,rancNaLouce,"Pamela");
    rancNaLouce->PrintInfo();

    delete rancUPodkovy;
    delete rancNaLouce;
    delete t;
    delete z;
    delete chk;
    delete kun1;
    return 0;



}